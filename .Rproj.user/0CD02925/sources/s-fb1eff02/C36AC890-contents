library(readr)
library("biomaRt")

source("3.Functions/2.Preprocessing/getArmLevelOfChromosomalEvent.R")
source("3.Functions/2.Preprocessing/getMutatedDriverGenes.R")
source("3.Functions/2.Preprocessing/getKaryobandMeta.R")

gene_meta <- read_tsv("1.Downloads/GRCh38.p12_export.tsv")

mutated_driver_genes <- getMutatedDriverGenes()
mutated_driver_genes$Chr_arm <- getArmLevelOfChromosomalEvent(mutated_driver_genes$Cytoband)
mutated_driver_genes$tsg <- grepl("tsg",mutated_driver_genes$`Tumor suppressor or oncogene prediction (by 20/20+)`)
mutated_driver_genes$tsg[is.na(mutated_driver_genes$`Tumor suppressor or oncogene prediction (by 20/20+)`)] <- NA


protein_coding_genes <- unique(subset(gene_meta, !is.na(`Protein stable ID`))$`Gene stable ID`) # N = 23,358
gene_meta <- unique(gene_meta[,c("Gene stable ID", "Chromosome/scaffold name","Gene start (bp)","Gene end (bp)","Strand","Karyotype band","Gene name")])
gene_meta <- gene_meta[!is.na(gene_meta$`Gene stable ID`) & !is.na(gene_meta$`Chromosome/scaffold name`) & !is.na(gene_meta$`Gene start (bp)`) & !is.na(gene_meta$`Gene end (bp)`) & !is.na(gene_meta$Strand) & !is.na(gene_meta$`Karyotype band`),]
gene_meta$protein_coding <- gene_meta$`Gene stable ID`%in% protein_coding_genes
gene_meta$chromosome_arm <- substr(gene_meta$`Karyotype band`,1,1)
gene_meta$band_number <- substr(gene_meta$`Karyotype band`,2,nchar(gene_meta$`Karyotype band`))
gene_meta$chromosome_number_and_arm <- NA
gene_meta$chromosome_number_and_karyotype <- NA
gene_meta$driver_gene <- gene_meta$`Gene name` %in% unique(mutated_driver_genes$Gene)
for (i in 1:nrow(gene_meta)){
  
  gene_meta$chromosome_number_and_arm[i] <- paste0(gene_meta$`Chromosome/scaffold name`[i], gene_meta$chromosome_arm[i])
  gene_meta$chromosome_number_and_karyotype[i] <- paste0(gene_meta$`Chromosome/scaffold name`[i], gene_meta$`Karyotype band`[i])
  
}

### Essential genes according to Cell publication:
# Cumulative haploinsufficiency and triplosensitivity drive aneuploidy patterns and shape the cancer genome

essential_145_Genes <-
  as.data.frame(readxl::read_xlsx("1.Downloads/ExternalAnalyses/Cumulative_haploinsufficiency_and_triplosensitivity_etc/Table_S5a_geneLists.xlsx", sheet = "Essential_genes_145"))

essential_332_Genes <-
  as.data.frame(readxl::read_xlsx("1.Downloads/ExternalAnalyses/Cumulative_haploinsufficiency_and_triplosensitivity_etc/Table_S5a_geneLists.xlsx", sheet = "Essential_genes_332"))

gene_meta$essential_145_gene <-
  gene_meta$`Gene name` %in% essential_145_Genes$Essential_genes_145 # 138 matching

gene_meta$essential_332_gene <-
  gene_meta$`Gene name` %in% essential_332_Genes$Essential_genes_332 # 318 matching

### Competing ref genes analysis: ###
Table_S4_alt_ref_genes <- read.csv("1.Downloads/ExternalAnalyses/Conventionally_used_reference_genes_are_not_outstanding/Table_S4.csv", sep=";")
Table_S4_alt_ref_genes[Table_S4_alt_ref_genes$Gene_name == "C10orf76","Gene_name"] <- "ARMH3"
Table_S4_alt_ref_genes[Table_S4_alt_ref_genes$Gene_name == "C19orf50","Gene_name"] <- "KXD1"
Table_S4_alt_ref_genes[Table_S4_alt_ref_genes$Gene_name == "C1orf212","Gene_name"] <- "SMIM12"
Table_S4_alt_ref_genes <- merge(x = Table_S4_alt_ref_genes, y = gene_meta[,c("Gene stable ID","Gene name")], by.x="Gene_name", by.y="Gene name", all.x=TRUE)
colnames(Table_S4_alt_ref_genes)[1] <- "Gene name"
gene_meta <- merge(x = gene_meta, y = Table_S4_alt_ref_genes, by = c("Gene name","Gene stable ID"))
gene_meta$Alternative_ref_gene <- !is.na(gene_meta$CV_pct)


### Add entrezgene_id ###
ensembl <- useMart("ensembl", dataset = "hsapiens_gene_ensembl")
mapping <- getBM(attributes = c("ensembl_gene_id", "hgnc_symbol","entrezgene_id"), mart = ensembl)
gene_meta <- merge(x=gene_meta, y=mapping, by.x="Gene stable ID", by.y="ensembl_gene_id")


saveRDS(gene_meta, file="2.Subassemblies/gene_meta.Rds")
saveRDS(mutated_driver_genes, file="2.Subassemblies/mutated_driver_genes.Rds")

karyoband_meta <- getKaryobandMeta(gene_meta)
saveRDS(karyoband_meta, file="2.Subassemblies/karyoband_meta.Rds")
