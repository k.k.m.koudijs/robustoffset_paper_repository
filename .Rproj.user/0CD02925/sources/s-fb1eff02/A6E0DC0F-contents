calculateSampleOffsets <- function(
  projects, 
  Included_samples, 
  Intra_project_sigma, 
  projects_mean_Intra_project_sigma.cutoff = 0.2
){
  
  dir.create(file.path("5.Final_output/1.Preprocessing", "Sample_offsets_figures"), showWarnings = FALSE)
  dir.create(file.path("5.Final_output/1.Preprocessing/Sample_offsets_figures", "Weighted_vs_unweighted_ranef"), showWarnings = FALSE)

  HKG_genes <- 
    unique(as.character(subset(Intra_project_sigma, projects_mean_Intra_project_sigma <= projects_mean_Intra_project_sigma.cutoff)$Gene))
  
  HKG_genexp_summary <- Intra_project_sigma[Intra_project_sigma$Gene %in% HKG_genes,]
  HKG_genexp_summary$Project <- factor(HKG_genexp_summary$Project, levels=rev(projects))
  
  printAndSavePlot(
    ggplot_object = ggplot(data=HKG_genexp_summary, aes(x=Intra_project_sigma, y=Project)) + geom_boxplot() + labs(
      x = expression(sigma[Intraproject]*' (log-scale)')
    ) + scale_x_log10(),
    path = "5.Final_output/1.Preprocessing/intra_project_sigma_HKG.png"
  )
  
  # GDC_PANCAN_htseq_counts Already on ln log-scale
  HKG_genexp <- turn_genexprMatrix_in_long_format(GDC_PANCAN_htseq_counts[rownames(GDC_PANCAN_htseq_counts) %in% HKG_genes,])
  HKG_genexp <- merge(x=HKG_genexp, y=Included_samples[,c("sample","Project")], by="sample")
  HKG_genexp <- merge(x=HKG_genexp, y=Intra_project_sigma[,c("Gene","Project","Inter_project_mean","RMA_mean_estimate")], by=c("Project","Gene"))
  HKG_genexp$norm_expr <- HKG_genexp$expr - HKG_genexp$Inter_project_mean
  
  for (i in 1:length(projects)){
    
    print(i)
    HKG_genexp_subset <- subset(HKG_genexp, Project==projects[i])
    
    model_unweighted <- lmer(norm_expr ~ (1|sample), data=HKG_genexp_subset)
    model_weighted <- lmer(norm_expr ~ (1|sample), weights = 1 / (RMA_mean_estimate^2), data=HKG_genexp_subset)
    
    ranef_unweighted <- ranef(model_unweighted)$sample
    ranef_weighted <- ranef(model_weighted)$sample
    
    colnames(ranef_unweighted)[1] <- "ranef_unweighted"
    colnames(ranef_weighted)[1] <- "ranef_weighted"
    
    ranef_combined <- cbind(ranef_unweighted, ranef_weighted)
    ranef_combined$sample <- rownames(ranef_combined)
    
    lm_summary <- summary(lm(ranef_weighted ~ ranef_unweighted, data=ranef_combined))
    
    printAndSavePlot(
      ggplot_object = ggplot(data=ranef_combined, aes(x=ranef_unweighted, y=ranef_weighted)) + geom_point() + geom_abline(color="black") + 
      labs(
        x = "Sample random effect calculated without using inverse variance weights",
        y = "Sample random effect calculated using inverse variance weights",
        title = paste0("TCGA project = ",projects[i],". Linear model R² = ", round(lm_summary$r.squared, digits=3))
      ) + geom_smooth(color="red"),
      path = paste0("5.Final_output/1.Preprocessing/Sample_offsets_figures/Weighted_vs_unweighted_ranef/",projects[i],".png")
    )
    
    ranef_combined$R2_weighted_vs_unweighted <- lm_summary$adj.r.squared
    ranef_combined$project_offset_unweighted <- coef(summary(model_unweighted))[1,1]
    ranef_combined$project_offset_unweighted_SE <- coef(summary(model_unweighted))[1,2]
    ranef_combined$project_offset_weighted <- coef(summary(model_weighted))[1,1]
    
    ranef_combined$project_plus_sample_offset_unweighted <- ranef_combined$project_offset_unweighted + ranef_unweighted$ranef_unweighted
    ranef_combined$project_plus_sample_offset_weighted <- ranef_combined$project_offset_weighted + ranef_weighted$ranef_weighted
    
    if (i == 1){
      
      ranef_combined_allprojects <- ranef_combined
      
    } else {
      
      ranef_combined_allprojects <- rbind(ranef_combined, ranef_combined_allprojects)
      
    }
    
  }
  
  # Remove columns if they already exist:
  Included_samples$R2_weighted_vs_unweighted <- NULL
  Included_samples$ranef_unweighted <- NULL
  Included_samples$ranef_weighted <- NULL
  Included_samples$project_offset_unweighted <- NULL
  Included_samples$project_offset_unweighted_SE <- NULL
  Included_samples$project_offset_weighted <- NULL
  Included_samples$project_plus_sample_offset_unweighted <- NULL
  Included_samples$project_plus_sample_offset_weighted <- NULL
  Included_samples <- merge(x=Included_samples, y=ranef_combined_allprojects, by="sample")
  
  ### Compare ranef SD with median Intra_project_sigma
  Project_median_Intra_project_sigma <- 
    aggregate(HKG_genexp_summary$Intra_project_sigma, by = list(HKG_genexp_summary$Project), FUN = median)
  
  colnames(Project_median_Intra_project_sigma) <- c("Project","median_Intra_project_sigma")
  
  Project_Ranef_sigma <- 
    aggregate(Included_samples$ranef_unweighted, by = list(Included_samples$Project), FUN = sd)
  colnames(Project_Ranef_sigma) <- c("Project","Project_Ranef_sigma")

  Project_Offset_median <- 
    aggregate(Included_samples$project_plus_sample_offset_unweighted, by = list(Included_samples$Project), FUN = median)
  colnames(Project_Offset_median) <- c("Project","Project_Offset_median")
  
  Project_median_Intra_project_sigma_vs_Project_Ranef_sigma <- 
    merge(x=Project_median_Intra_project_sigma, y=Project_Ranef_sigma, by="Project")
  
  Project_median_Intra_project_sigma_vs_Project_Ranef_sigma <- 
    merge(x=Project_median_Intra_project_sigma_vs_Project_Ranef_sigma, y=Project_Offset_median, by="Project")
  
  Included_samples$`Increased ploidy` <-
    Included_samples$`Genome doublings` > 0
  
  Included_samples$`Fraction of intact tumor DNA` <-
    Included_samples$Fraction_intact_tumor_DNA
  
  printAndSavePlot(
    ggplot_object = ggplot(data=Included_samples, aes(x=Project_rev, y=exp(project_plus_sample_offset_unweighted))) + geom_boxplot() + coord_flip() + labs(
      x = "Project",
      y = "Project offset + sample offset"
    ) + scale_y_log10(),
    path = paste0("5.Final_output/1.Preprocessing/Sample_offsets_figures/Sample_offsets_by_project.png")
  )
  
  output <- list()
  output$Included_samples <- Included_samples
  output$Project_median_Intra_project_sigma_vs_Project_Ranef_sigma <- Project_median_Intra_project_sigma_vs_Project_Ranef_sigma
  
  return(output)
}