createMatrixSampleCNV <- function(input, project_tumor_sample_info, cnv_location){
  
  o <- as.data.frame(matrix(nrow = nrow(project_tumor_sample_info), ncol=1+length(unique(input[,cnv_location]))), stringsAsFactors = FALSE)
  colnames(o) <- c("sample",unique(input[,cnv_location]))
  o$sample <- project_tumor_sample_info$sample
  
  for (i in 1:nrow(o)){
    
    df <- subset(input, Tumor_Sample_Barcode==o[i,"sample"])
    
    if (subset(project_tumor_sample_info, sample==o[i,"sample"])$cnv_available == FALSE){
      
      o[i,2:ncol(o)] <- NA
      
    } else {
      
      o[i,2:ncol(o)] <- FALSE
      
      if (nrow(df) > 0){
        
        o[i,unique(df[,cnv_location])] <- TRUE
        
      }
      
    }

  }
  
  return(o)
}