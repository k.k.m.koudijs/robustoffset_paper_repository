getProjectOffsetEstimates <- function(Included_samples, Intra_project_sigma, GDC_PANCAN_htseq_counts){
  
  
  projects <- unique(Intra_project_sigma$Project)
  methods <- c("Using_all_tumor_types","Using_different_primary_organ")
  
  Project_Organ_mapping <- 
    as.data.frame(readxl::read_excel(path="2.Subassemblies/TCGA_to_GTEx_mapping.xlsx"))
  
  output <- data.frame(
    Project = rep(projects, times=2),
    Method = rep(methods, each=length(projects)),
    Estimated_bias_mean = NA,
    Estimated_bias_stdev = NA,
    stringsAsFactors = FALSE
  )
  
  for (i in 1:nrow(output)){
    
    p <- output[i,"Project"]
    m <- output[i,"Method"]
    
    if (m == "Using_all_tumor_types"){
      
      temp <- 
        getInterprojectMeanAndSigma(
          sample_meta = Included_samples, 
          log_genexp = GDC_PANCAN_htseq_counts, 
          included_projects = projects
        )
      
    } else if (m == "Using_different_primary_organ"){
      
      project_organ <- subset(Project_Organ_mapping, TCGA_project == p)$PrimaryOrgan
      projects_subset <- subset(Project_Organ_mapping, PrimaryOrgan != project_organ)$TCGA_project
      
      temp <- 
        getInterprojectMeanAndSigma(
          sample_meta = Included_samples, 
          log_genexp = GDC_PANCAN_htseq_counts, 
          included_projects = projects_subset
        )
      
    }
    
    input <- subset(Intra_project_sigma, Project == p, select = c("Gene","Intra_project_mean","Intra_project_sigma"))  
    input <- merge(x = input, y = temp, by = "Gene")
    input$Delta_project_mean <- input$Intra_project_mean - input$Inter_project_mean
    
    pg <- ggplot_build(
      ggplot(data = input, 
             aes(x = Inter_project_sigma, y = Delta_project_mean, weight= 1 / (Intra_project_sigma^2))) + 
        geom_smooth() 
    )
    
    output[i,c("Estimated_bias_mean","Estimated_bias_stdev")] <-
      as.numeric(pg$data[[1]][1,c(2, 5)])
    
  }
  
  return(output)
  
}