recalculate_Inter_project_sigma <- function(Inter_project_sigma, Included_samples, ProjectOffsetEstimates){
  
  Inter_project_sigma$Inter_project_mean_corrected <- NA
  Inter_project_sigma$Inter_project_sigma_corrected <- NA
  
  for (i in 1:nrow(Inter_project_sigma)){
    
    print(i)
    log_CPMs <- as.data.frame(GDC_PANCAN_htseq_counts[rownames(GDC_PANCAN_htseq_counts) == Inter_project_sigma$Gene[i],])
    
    colnames(log_CPMs)[1] <- "log_CPM"
    log_CPMs$sample <- rownames(log_CPMs)
    
    log_CPMs <- merge(x=log_CPMs, y=Included_samples[,c("sample","disease_code.project")], by="sample")
    
    log_CPMs <- merge(x=log_CPMs, y=subset(ProjectOffsetEstimates, Method=="Using_different_primary_organ"), by.x="disease_code.project", by.y="Project")
    log_CPMs$log_CPM_corrected <- log_CPMs$log_CPM - log_CPMs$Estimated_bias_mean
    
    
    logCPM_means_corrected <- aggregate(log_CPMs[, c("log_CPM_corrected")], list(log_CPMs$disease_code.project), mean)
    Inter_project_sigma[i,"Inter_project_mean_corrected"] <- mean(logCPM_means_corrected$x)
    Inter_project_sigma[i,"Inter_project_sigma_corrected"] <- sd(logCPM_means_corrected$x)
    
  }
  
  return(Inter_project_sigma)
  
}